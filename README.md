# yew-scanner

A simple qrcode scanner to be used in a [yew](https://yew.rs/) context.

## Getting started

Just add this to your project
```toml
[dependencies]
yew = "0.19"
yew-scanner = "0.1"

```

And put this somewhere into your yew-html macro
```rust
    <Scanner onscan={ ctx.link().callback(|s: String| { /* your event handler goes here */ }) } />
```

## License
Open Source using the MIT License

